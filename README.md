# dev_stack

Start modifying the planBLICK stack easily with this repository. 

## Usage

1. Checkout this repository
2. Run `install.ps1`. If this causes problems, see the troubleshooting-section.
3. Once the WASP-GUI opens in your browser, click the "Beginn stack installation"-link
4. Wait for the installation to finish. Depending on your machine, this can take a few minutes.
5. Once the installer finished, a new page will be opened in your browser with the login-page. Login with username/password:  `superuser/pb2019!`
6. (Optional) Once you've done testing the stack, uninstall everything again by running `uninstall.ps1`

## Other ressources:
Konga (GUI for the Kong-Gateway): http://localhost:1337/

## Notice
This repository is currently under heavy development and gets constantly updates. So please pull frequently to get the latest version in time. 

## Feedback
If you have any suggestions, bugs or pull requests, feel free to contact us. 

## Wanna join forces?
We are happy to join forces with likeminded developers, but be aware: We follow a strict "no bullshit" policy. This means:
- We won't do things because they are trending or follow hypes. We do things because we believe that they have to be done to achieve the best possible software. 
- The best possible software in our definition is software which helps the user to solve his problems not the one which satisfy developer needs the most. 
- We follow "less is more" and "simple is better than complex" principles in our code. We like our code to be friendly to beginners, yet powerfull.
- We believe in the [Manifesto for Agile Software Development](https://agilemanifesto.org/)

You think the same? Great, let's work together. 
You think we are nuts? Still great, but better let's agree to disagree and work on different projects. 

Happy coding!

## Troubleshooting
If you cannot run the install.ps1 or the uninstall.ps1 you'll probably do not allow execution of scripts which are not signed. 
To allow running both of these script simply run:

`Unblock-File -Path .\install.ps1`

`Unblock-File -Path .\uninstall.ps1`

If this still doesn't work, just start a new powershell with:
`powershell.exe -ExecutionPolicy Bypass`

In the now opened powershell install.ps1 and uninstall.ps1 should be executable.

