/**
* JetBrains Space Automation
* This Kotlin-script file lets you automate build activities
* For more info, see https://www.jetbrains.com/help/space/automation.html
*/

// STAGING
job("Build, push and deploy staging") {
    startOn {
        gitPush {
            branchFilter {
                +Regex("staging")
            }
        }
    }
    container(displayName = "Set deployment", image = "openjdk:11") {
        kotlinScript { api ->
            api.space().projects.automation.deployments.start(
                project = api.projectIdentifier(),
                targetIdentifier = TargetIdentifier.Key("wasp-dev-staging"),
                version = System.getenv("JB_SPACE_GIT_REVISION"),
                syncWithAutomationJob = true
            )
        }
    }
    docker {
        build {
            file = "./Dockerfile"
            labels["vendor"] = "planBLICK GmbH"
        }
        push("planblick.registry.jetbrains.space/p/crowdsoft/images/wasp-dev") {
            tags("\$JB_SPACE_GIT_REVISION", "staging")
        }
    }
}

// PRODUCTION
job("Build, push and deploy production") {
    startOn {
        gitPush {
            branchFilter {
                +Regex("production")
            }
        }
    }
    container(displayName = "Set deployment", image = "openjdk:11") {
        kotlinScript { api ->
            api.space().projects.automation.deployments.start(
                project = api.projectIdentifier(),
                targetIdentifier = TargetIdentifier.Key("wasp-dev-production"),
                version = System.getenv("JB_SPACE_GIT_REVISION"),
                // automatically update deployment status based on a status of a job
                syncWithAutomationJob = true
            )
        }
    }
    docker {
        build {
            file = "./Dockerfile"
            labels["vendor"] = "planBLICK GmbH"
        }
        push("planblick.registry.jetbrains.space/p/crowdsoft/images/wasp-dev") {
            tags("\$JB_SPACE_GIT_REVISION", "production")
        }
    }
}

