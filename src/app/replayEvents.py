from amqp import client
import json
import datetime



def replayEvents():
    begin_time = datetime.datetime.now()
    mq = client()

    ############## Full stream
    with open("/src/app/eventstream.json", "r") as f:
        data = f.read()
        rows = json.loads(data).get("rows")

    counter = 0
    timeshift = None
    for row in rows:
        print(row.get("type"))
        counter = counter + 1
        type = row.get("type")

        payload = row.get("payload")
        if isinstance(payload, str):
            payload = json.loads(payload)


        offset_in_days = 0
        if row.get("type") in ["newAppointmentCreated", "appointmentChanged"]:
            starttime = datetime.datetime.strptime(payload.get("starttime"), '%Y-%m-%d %H:%M:%S')
            endtime = datetime.datetime.strptime(payload.get("endtime"), '%Y-%m-%d %H:%M:%S')
            duration = endtime - starttime
            if timeshift is None:
                timeshift = datetime.datetime.now().date() - starttime.date()

            newstart = datetime.datetime.combine(starttime.date() + timeshift, datetime.time(starttime.hour, starttime.minute)) + + datetime.timedelta(days=offset_in_days)
            newend = newstart + duration

            payload["starttime"] = newstart.strftime('%Y-%m-%d %H:%M:%S')
            payload["endtime"] = newend.strftime('%Y-%m-%d %H:%M:%S')
            print("Starttime", starttime)
            print("Timeshift", timeshift)
            print("Newstart", newstart)

        print(counter, payload.get("correlation_id"))
        payload = json.dumps(payload)

        mq.publish(toExchange="message_handler", routingKey=type, message=payload)

    print(f"Script took: {datetime.datetime.now() - begin_time} to finish.")