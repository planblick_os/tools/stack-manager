import subprocess
import sys
import os
import shutil
import requests
import json

from time import sleep

class WASPInstaller():
    def __init__(self, socketio):
        self.io = socketio
        self.repos_checked_out = False

    def install(self):
        self.checkoutRepositories()

    def bootstrap(self):
        self.sio.emit('addLog', {"message": f"Enabling correlationId plugin..."})
        url = "http://stack-bootstrapper.planblick.svc/start-bootstrapping"
        payload = json.dumps({
            "name": "correlation-id",
            "config": {
                "header_name": "X-Correlation-ID",
                "generator": "uuid",
                "echo_downstream": True
            }
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        self.sio.emit('addLog', {"message": f"CorrelationId-plugin enabling gateway response (Code " + str(
            response.status_code) + " ): <pre>" + response.text + "</pre>"})


    def checkoutRepositories(self, skipIfExists=True):
        if not (skipIfExists and os.path.exists("/src/app/repos")):
            if os.path.exists("/src/app/repos"):
                shutil.rmtree("/src/app/repos")
            sleep(1)
            os.makedirs("/src/app/repos")

            f = open('/src/app/repos.json')
            repos = json.load(f)

            for repo in repos:
                self.checkoutRepo(repo)

        self.repos_checked_out = True
        return

    def checkoutRepo(self, repo_url):
        self.io.emit('addLog', {"message": f"Start cloning {repo_url}"})
        if os.getenv("GIT_USERNAME") and os.getenv("GIT_PASSWORD"):
            repo_url = repo_url.replace("https://", f"https://{os.getenv('GIT_USERNAME')}:{os.getenv('GIT_PASSWORD')}@")

        command = ["git", "clone", repo_url, "--progress", "--recurse-submodules"]
        process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd="/src/app/repos")
        while True:
            output = process.stdout.readline()
            if process.poll() is not None and output == b'':
                break
            if output:
                self.io.emit('addLog', {"message": output.strip().decode(), "id": repo_url})
                print(output.strip())

        retval = process.poll()
        if retval == 0:
            self.io.emit('addLog', {"message": f"Cloned successfully"})
        else:
            self.io.emit('addLog', {"message": f"Error while cloning {repo_url}"})

        return retval
