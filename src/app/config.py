class Config():
    host = 'localhost'
    username = 'root'
    password = "password"
    port = "3306"
    base_url = "http://localhost:8001"


class AmqpConfig:
    host = "host.docker.internal"
    username = "developer"
    password = "password"
    port = 5672
    vhost = "/"