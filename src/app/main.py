#!/usr/bin/env python
import eventlet
eventlet.monkey_patch()

from threading import Lock
from flask import Flask, render_template, session, request, \
    copy_current_request_context, jsonify
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect, Namespace
import json
from flask_cors import CORS
from waspinstaller import WASPInstaller
import threading
import replayEvents


async_mode = "eventlet"

app = Flask(__name__, static_folder='html', static_url_path='')

socketio = SocketIO(app, cookie=False, async_mode=async_mode, engineio_logger=False, origins='*', cors_allowed_origins=[])

app.config['CORS_SUPPORTS_CREDENTIALS'] = True
CORS(app, resources={r"/*": {"origins": "*"}}, automatic_options=True)

t = None

@app.route('/start-install', methods=["GET"])
def start_install():
    global t
    socketio.emit('addLog', {"message": "Starting installation"})
    t = threading.Thread(name='child procs', target=WASPInstaller(socketio).install)
    t.start()

    return jsonify({"message": "OK"}), 200

@app.route('/start-bootstrap', methods=["GET"])
def bootstrap():
    global t
    socketio.emit('addLog', {"message": "Starting installation"})
    t = threading.Thread(name='child procs', target=WASPInstaller(socketio).bootstrap)
    t.start()

    return jsonify({"message": "OK"}), 200

@app.route('/start-replay', methods=["GET"])
def replay():
    socketio.emit('addLog', {"message": "Starting replay"})

    replayEvents.replayEvents()

    return jsonify({"message": "OK"}), 200

@app.route('/repos-available', methods=["GET"])
def repos_available():
    global t
    if t is None or t.is_alive():
        return jsonify({"message": "NOK"}), 202
    else:
        return jsonify({"message": "OK"}), 205

@app.route('/status', methods=["GET"])
def status():
    return jsonify({"message": "OK"}), 200

@app.route('/healthz', methods=["GET"])
def health_action():
    return jsonify({"message": "OK"}), 200


@socketio.on('conPing', namespace='/')
def ping_pong():
    emit('conPong')

@socketio.on('addLog', namespace='/')
def addLog(message):
    print("Got addlog", message)
    emit('addLog', message, broadcast=True)


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', debug=True)