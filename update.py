import os
import sys
import json
import subprocess
import shutil

if input("Running this script will potentially reset all open changes you have. Are you sure you want to continue? [y/n]").lower() not in ('y', 'yes'):
    sys.exit()

print("Updating myself")
print("... fetching latest ...")
command = ["git", "fetch", "--all"]
process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
print("... checking out master-branch ...")
command = ["git", "checkout", "master"]
process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
print("... resetting master-branch hard ...")
command = ["git", "reset", "--hard"]
process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
print("------- DONE -------")

f = open('./src/app/repos.json')
repo_list = json.load(f)
repos = [(url, url.replace(".git", "").split("/").pop()) for url in repo_list]

if not os.path.exists("./src/app/repos"):
    os.makedirs("./src/app/repos")

for repo_url, repo_name in repos:
    print("----------------------------------")
    print("Processing repo " + repo_name)
    if not os.path.exists("./src/app/repos/" + repo_name):
        print("New repo...")
        print("... cloning ...")
        command = ["git", "clone", repo_url, "--progress", "--recurse-submodules"]
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd="./src/app/repos")
        while True:
            output = process.stdout.readline()
            if process.poll() is not None and output == b'':
                break
            if output:
                print(output)
        print("------- DONE -------")
    else:
        print("Updating repo...")
        print("... fetching latest ...")
        command = ["git", "fetch", "--all"]
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        print(process.communicate())
        print("... checking out master-branch ...")
        command = ["git", "checkout", "master"]
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                   cwd="./src/app/repos/" + repo_name)
        print(process.communicate())
        print("... resetting master-branch hard ...")
        command = ["git", "reset", "--hard", "origin/master"]
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                   cwd="./src/app/repos/" + repo_name)
        print(process.communicate())

        print("... pulling ...")
        command = ["git", "pull"]
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd="./src/app/repos/" + repo_name)
        print(process.communicate())

        print("... updating submodule ...")
        command = ["git", "submodule", "update"]
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                   cwd="./src/app/repos/" + repo_name)
        print(process.communicate())

        print("------- DONE -------")


print("\r\n\r\nChange to './src/app/repos/dev-stack' now and (re-)start the stack now with 'docker-compose up -d --build' and wait for everything to run properly.")
print("If errors occur, try to run docker compose up again (can be without the --build-flag now)")
print("If you are doing a manual install instead of an update do the following instead:")
print("   1. Login to docker-registry (docker login planblick.registry.jetbrains.space).\r\n")
print("   2. Make sure WASP container is running (docker-compose up in the WASP main folder were this file resides in).\r\n")
print("   3. Open these urls in your browser:")
print("      - http://localhost:8021/start-bootstrapping")
print("      - http://localhost:8080/start-replay")
print("Your frontend should be available now at http://localhost:7070")
print("If you cannot login, go to http://localhost:1337 login with admin/password, go to consumer->superuser->credentials and delete and recreate superuser basic-auth.")
