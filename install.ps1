$us = Read-Host 'Enter your git-user-name'
$pw = Read-Host 'Enter your git-password'

((Get-Content -path ./.env_distributed -Raw) -replace '{user}', $us) | Set-Content -Path ./.env
((Get-Content -path ./.env -Raw) -replace '{password}', $pw) | Set-Content -Path ./.env

docker network create kong-net
docker-compose build
docker-compose up -d
do {
  Write-Host "waiting for WASP GUI to become available..."
  sleep 3
} until(Test-NetConnection 127.0.0.1 -Port 8080 -InformationLevel Quiet)
start "http://localhost:8080/install.html"
Write-Host "Open http://localhost:8080/install.html now in your browser"


# Wait until the installer-script checked out all repositories
do {
  sleep 3
  Write-Host "waiting until repos are available"
  $HTTP_Request = [System.Net.WebRequest]::Create('http://localhost:8080/repos-available')
  $HTTP_Response = $HTTP_Request.GetResponse()
  $HTTP_Status = [int]$HTTP_Response.StatusCode
  If ($HTTP_Response -eq $null) { }
  Else { $HTTP_Response.Close() }

} until($HTTP_Status -eq 205)

Copy-Item -Path ".\src\app\dummy-certs\account-manager" -Destination ".\src\app\repos\certificate-store\src\certs\account-manager" -Recurse

Write-Host "Starting development-stack"
cd ./src/app/repos/dev-stack
docker-compose down
docker-compose build
docker-compose up -d
docker-compose up -d
#Get-ChildItem -Directory | ForEach-Object { docker-compose -f ./$($_.Name)/docker-compose.yml up -d }
cd ..
cd ..
cd ..
cd ..

# Wait until the installer-script checked out all repositories
do {
  sleep 3
  Write-Host "trying to start bootstrapping (http://localhost:8021/start-bootstrapping)"

  $HTTP_Request = [System.Net.WebRequest]::Create('http://localhost:8021/start-bootstrapping')
  $HTTP_Response = $HTTP_Request.GetResponse()
  $HTTP_Status = [int]$HTTP_Response.StatusCode
  If ($HTTP_Response -eq $null) { }
  Else { $HTTP_Response.Close() }

} until($HTTP_Status -eq 200)

do {
  Write-Host "waiting for Example Frontends to become available..."
  sleep 3
} until(Test-NetConnection 127.0.0.1 -Port 7070 -InformationLevel Quiet)


Write-Host "trying to start replaying (http://localhost:8080/start-replay)"
$HTTP_Request = [System.Net.WebRequest]::Create('http://localhost:8080/start-replay')
$HTTP_Response = $HTTP_Request.GetResponse()
$HTTP_Status = [int]$HTTP_Response.StatusCode
If ($HTTP_Response -eq $null) { }
Else { $HTTP_Response.Close() }

start "http://localhost:7070/"


[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')
[System.Windows.Forms.MessageBox]::Show('System is now ready. Login on the page which was just opened in your browser (http://localhost:7070/) with user "superuser" and password "pb2019!"')

