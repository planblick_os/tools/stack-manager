docker-compose down -v

cd ./src/app/repos/dev-stack
docker-compose down -v
docker network rm kong-net

cd ../../
rm -r -fo ./repos
cd ..
cd ..